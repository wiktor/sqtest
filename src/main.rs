use sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::serialize::SerializeInto;

fn main() -> openpgp::Result<()> {
    let (cert, _rev) =
        CertBuilder::new()
            .add_userid("Alice Lovelace <alice@lovelace.name>")
            .add_signing_subkey()
            .add_transport_encryption_subkey()
            .add_storage_encryption_subkey()
            .generate()?;

    let armored = String::from_utf8(cert.armored().to_vec()?)?;
    println!("{}", armored);
    Ok(())
}
